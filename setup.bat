@echo off

echo "Ensure you have Python installed before running this script."
echo "Refer to PDFProcessor.py for usage and examples"

REM Create and activate the virtual environment
python -m venv venv
call venv\Scripts\activate

REM Install the required packages
pip install -r requirements.txt
