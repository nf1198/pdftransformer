import os
import click
from PyPDF2 import PdfReader, PdfWriter
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from reportlab.lib.colors import Color
from io import BytesIO

"""
This script will add watermarks to all PDFs in a specified folder.

1) Activate the Virual Environment using scripts/activate
2) Run the script from the script folder using:
     python .\\PDFProcessor.py --text "Watermark Text" --input-dir "<abs path to input>" --output-dir "<abs path to output>\\processed" --color "0000FF" --x-mm 30
"""

class PDFFile:
    def __init__(self, path):
        self.path = path
        self.name = os.path.basename(path)
        self.reader = PdfReader(path)
        self.writer = PdfWriter()

    def apply_transformations(self, transformations):
        for page_num in range(len(self.reader.pages)):
            page = self.reader.pages[page_num]
            for transformation in transformations:
                transformation.apply(page, self.name, page_num)
            self.writer.add_page(page)

    def save(self, output_path):
        with open(output_path, 'wb') as output_file:
            self.writer.write(output_file)

class PDFTransform:
    def apply(self, page, pdf_name, page_num):
        raise NotImplementedError("Subclasses should implement this method")

class Position:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    @staticmethod
    def from_mm(x_mm, y_mm, ref, page_size):
        x = x_mm * 2.835
        y = y_mm * 2.835
        page_width = float(page_size[0])
        page_height = float(page_size[1])
        if ref == 'TOP-LEFT':
            return Position(x, page_height - y)
        elif ref == 'TOP-RIGHT':
            return Position(page_width - x, page_height - y)
        elif ref == 'BOTTOM-LEFT':
            return Position(x, y)
        elif ref == 'BOTTOM-RIGHT':
            return Position(page_width - x, y)
        else:
            raise ValueError("Invalid reference point")

    def calculate_position(self, page_width, page_height):
        return self.x, self.y

class WatermarkTransformation(PDFTransform):
    def __init__(self, text, color, style, position, rect_color='FFFFFF', rect_opacity=0.5):
        self.text = text
        self.color = color
        self.style = style
        self.position = position
        self.rect_color = rect_color
        self.rect_opacity = rect_opacity

    def create_watermark(self, page_size):
        packet = BytesIO()
        can = canvas.Canvas(packet, pagesize=page_size)
        
        # Convert text color from HEX to RGB
        r, g, b = [int(self.color[i:i+2], 16) / 255.0 for i in (0, 2, 4)]
        
        # Convert rectangle color from HEX to RGB
        rect_r, rect_g, rect_b = [int(self.rect_color[i:i+2], 16) / 255.0 for i in (0, 2, 4)]
        rect_color = Color(rect_r, rect_g, rect_b, alpha=self.rect_opacity)

        # Set font style
        if "bold" in self.style and "italic" in self.style:
            can.setFont("Helvetica-BoldOblique", 12)
        elif "bold" in self.style:
            can.setFont("Helvetica-Bold", 12)
        elif "italic" in self.style:
            can.setFont("Helvetica-Oblique", 12)
        else:
            can.setFont("Helvetica", 12)

        x, y = self.position.calculate_position(*page_size)
        text_width = can.stringWidth(self.text, "Helvetica", 12)
        text_height = 12

        # Draw the rectangle with transparency
        can.setFillColor(rect_color)
        can.setStrokeColor(rect_color)
        can.rect(x - 2, y - 2, text_width + 4, text_height + 4, stroke=0, fill=1)

        # Draw the text with full opacity
        can.setFillAlpha(1.0)  # Ensure full opacity for text
        can.setFillColorRGB(r, g, b)
        can.drawString(x, y, self.text)
        can.save()
        packet.seek(0)
        return PdfReader(packet)

    def apply(self, page, pdf_name, page_num):
        page_size = (float(page.mediabox[2]), float(page.mediabox[3]))
        watermark = self.create_watermark(page_size)
        page.merge_page(watermark.pages[0])


class PDFLoader:
    @staticmethod
    def load_pdfs(input_dir):
        pdf_files = []
        for filename in os.listdir(input_dir):
            if filename.endswith(".pdf"):
                pdf_files.append(PDFFile(os.path.join(input_dir, filename)))
        return pdf_files

@click.command()
@click.option('--text', default='', help='Text to add to each page.')
@click.option('--input-dir', default='.', help='Directory containing PDF files to process.')
@click.option('--output-dir', default='./processed', help='Directory to save processed PDF files.')
@click.option('--color', default='000000', help='Color of the text in HEX format (default is black).')
@click.option('--style', default='', help='Style of the text (options: bold, italic, both).')
@click.option('--location', default='TOP-LEFT', help='Position of the text (e.g., TOP-LEFT, TOP-RIGHT, BOTTOM-LEFT, BOTTOM-RIGHT).')
@click.option('--x-mm', default=10, help='X offset in millimeters.')
@click.option('--y-mm', default=10, help='Y offset in millimeters.')
@click.option('--rect-color', default='FFFFFF', help='Color of the rectangle in HEX format (default is white).')
@click.option('--rect-opacity', default=0.75, help='Opacity of the rectangle (default is 0.5).')
def main(text, input_dir, output_dir, color, style, location, x_mm, y_mm, rect_color, rect_opacity):
    pdf_files = PDFLoader.load_pdfs(input_dir)
    
    for pdf_file in pdf_files:
    
        first_page = pdf_file.reader.pages[0]
        page_size = (first_page.mediabox[2], first_page.mediabox[3])
        position = Position.from_mm(x_mm, y_mm, location, page_size)
    
        # Initialize transformations
        transformations = []
        position = Position.from_mm(x_mm, y_mm, location, page_size)  # Dummy size for initialization
        transformations.append(WatermarkTransformation(text, color, style, position))
        
        pdf_file.apply_transformations(transformations)
        output_path = os.path.join(output_dir, pdf_file.name)
        pdf_file.save(output_path)
    
    click.echo(f"Processed {len(pdf_files)} PDF files.")

if __name__ == '__main__':
    main()
